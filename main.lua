require "labyrinth"
require "gamemanager"

local gm = GameManager:new()
gm:init()

function love.load()
  gm:load()
end

function love.draw()
  gm:draw()
end

function love.update(dt)
  gm:update(dt)
end
