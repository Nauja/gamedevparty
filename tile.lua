Tile = {}
Tile.__index = Tile

function Tile:new()
  local o = {_type = 0, _texture = 0, _object = 0}
  setmetatable(o, self)
  return o
end

function Tile:type()
  return self._type
end

function Tile:setType(type)
  self._type = type
end

function Tile:texture()
  return self._texture
end

function Tile:setTexture(texture)
  self._texture = texture
end

function Tile:object()
  return self._object
end

function Tile:setObject(object)
  self._object = object
end
