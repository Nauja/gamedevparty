require "tile"

Labyrinth = {}
Labyrinth.__index = Labyrinth

function Labyrinth:load(filename)
  local f = assert(io.open(filename, "r"))
  local w = f:read("*n")
  print(w)
  local h = f:read("*n")
  print(h)
  local lab = Labyrinth:new(w, h)
  for i = 1, 3 do
    print(f:read("*line"))
  end
  for y = 1, h do
    for x = 1, w do
      local c = f:read(1)
      io.write(c)
      lab._tiles[y][x]:setType(tonumber(c))
    end
    print(f:read("*line"))
  end
  for i = 1, 2 do
    print(f:read("*line"))
  end
  for y = 1, h do
    for x = 1, w do
      local c = f:read(1)
      io.write(c)
      lab._tiles[y][x]:setTexture(c)
    end
    print(f:read("*line"))
  end
  for i = 1, 2 do
    print(f:read("*line"))
  end
  for y = 1, h do
    for x = 1, w do
      local c = f:read(1)
      io.write(c)
      lab._tiles[y][x]:setObject(c)
    end
    print(f:read("*line"))
  end
  f:close(f)
  return lab
end

function Labyrinth:new(width, height)
  local o = {_width = width, _height = height, _tiles = {}}
  setmetatable(o, self)
  for j = 1, height do
    o._tiles[j] = {}
    for i = 1, width do
      o._tiles[j][i] = Tile:new()
    end
  end
  return o
end

function Labyrinth:width()
  return self._width
end

function Labyrinth:height()
  return self._height
end

function Labyrinth:tiles()
  return self._tiles
end

function Labyrinth:tile(x, y)
  if x > 0 and y > 0 and x <= self._width and y <= self._height then
    return self._tiles[y][x]
  else
    return nil
  end
end

function Labyrinth:print()
  print(self:width())
  print(self:height())
  print("Type:")
  for j = 1, self._height do
    for i = 1, self._width do
      io.write(self._tiles[j][i]:type())
    end
    print()
  end
  print("Texture:")
  for j = 1, self._height do
    for i = 1, self._width do
      io.write(self._tiles[j][i]:texture())
    end
    print()
  end
  print("Object:")
  for j = 1, self._height do
    for i = 1, self._width do
      io.write(self._tiles[j][i]:object())
    end
    print()
  end
end
